import React from "react";
import { Route } from "react-router-dom";
import NavBar from "./components/NavBar";
import Photos from "./pages/Photos";
import Cart from "./pages/Cart";
import { ContextProvider } from "./Context";

function App() {
  return (
    <ContextProvider>
      <NavBar />
      <Route exact path="/" component={Photos} />
      <Route exact path="/cart" component={Cart} />
    </ContextProvider>
  );
}

export default App;

import React, { useContext } from "react";

import Image from "../components/Image";
import { Context } from "../Context";

function Photos() {
  const { photos } = useContext(Context);

  return (
    <div className="flex flex-wrap justify-start mt-4">
      {photos &&
        photos.map((picture) => {
          return <Image picture={picture} key={picture.id} />;
        })}
    </div>
  );
}

export default Photos;

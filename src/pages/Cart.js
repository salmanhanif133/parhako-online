import React, { useContext } from "react";

import CartItem from "../components/CartItem";
import CartPrice from "../components/CartPrice";
import { Context } from "../Context";

function Cart() {
  const {
    cartItems,
    removeAllFromCart,
    placeOrder,
    order,
    placed,
  } = useContext(Context);

  return (
    <div className={`flex flex-grow opacity ${!order && "cursor-wait"}`}>
      <div className="w-9/12 bg-gray-100 flex flex-col pb-4">
        {cartItems.length > 0 ? (
          <>
            <div className="flex flex-wrap justify-start mt-4">
              {cartItems.map((item) => {
                return <CartItem item={item} />;
              })}
            </div>
            <div className="text-center mt-auto mb-8">
              <button
                className="py-2 px-4 text-lg rounded-lg bg-red-600 text-white font-semibold inline-flex items-center"
                onClick={() => removeAllFromCart()}
              >
                <span>Remove All from cart</span>
              </button>
            </div>
          </>
        ) : (
          <p className="text-4xl text-center py-4">Your cart is empty</p>
        )}
      </div>

      <div className="w-1/4 bg-white shadow-lg p-4 flex flex-col">
        {cartItems.length > 0 ? (
          <>
            <p className="text-xl font-semibold text-center text-gray-900">
              Cart Item
            </p>
            <div className="mt-4">
              {cartItems.map((item) => {
                return <CartPrice item={item} />;
              })}
            </div>
            <div className="mt-auto mb-8">
              <div className="flex items-center justify-between">
                <p className="text-xl font-semibold text-center text-gray-900">
                  Total Price
                </p>
                <p>$69</p>
              </div>
              <div className="text-center mt-4">
                {order ? (
                  placed ? (
                    <button className="py-2 px-4 text-lg rounded-lg  mx-auto bg-green-400  focus:outline-none text-white font-semibold text-center">
                      Placed!
                    </button>
                  ) : (
                    <button
                      className="py-2 px-4 text-lg rounded-lg  mx-auto bg-blue-500  hover:bg-blue-700 focus:outline-none text-white font-semibold text-center"
                      onClick={() => placeOrder()}
                    >
                      Place Order
                    </button>
                  )
                ) : (
                  <button className="py-2 px-4 text-lg rounded-lg  mx-auto bg-orange-400 cursor-wait focus:outline-none text-white font-semibold text-center">
                    Placing...
                  </button>
                )}
              </div>
            </div>
          </>
        ) : (
          <p className="text-xl font-semibold text-center text-gray-900">
            Cart Empty
          </p>
        )}
      </div>
    </div>
  );
}

export default Cart;

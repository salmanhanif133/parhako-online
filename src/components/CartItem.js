import React, { useContext } from "react";
import { Context } from "../Context";

const CartItem = ({ item }) => {
  const { removeFromCart } = useContext(Context);

  return (
    <div className="relative p-8 pt-0 z-10 rouned-lg">
      <img src={item.url} alt={item.url} className="h-64 w-64 rounded-lg" />
      <div className="absolute left-0 top-0 p-8 pt-0">
        <div className="h-64 w-64 bg-white opacity-0 hover:opacity-50 transition duration-500 ease-in-out z-0">
          <div className="absolute left-0 top-0 p-8 pt-2 ml-6 h-6 w-6">
            <div className="flex w-16 items-center justify-between">
              <svg
                className="w-6 h-6 fill-current text-green-700 stroke-current stroke-2"
                onClick={() => removeFromCart(item, item.id)}
                fill="none"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path>
              </svg>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CartItem;
